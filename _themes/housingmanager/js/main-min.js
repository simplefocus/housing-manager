$("img.lazy").lazyload({
    // Lazy load!
    effect : "fadeIn"
});


// Vertical align plugin
// -------------------------

(function ($) {
$.fn.vAlign = function() {
  return this.each(function(i){
  var ah = $(this).height();
  var ph = $(this).parent().height();
  var mh = ((ph-ah) / 2) - 100;
  $(this).css('margin-top', mh);
  });
};
})(jQuery);


$(document).ready(function(){

  if($(window).width() < 768) {
    $('#main-nav .control').on("click", function(e){
      e.preventDefault();
      $('#main-nav .main-nav, #home, #interior').toggleClass('open');
    });
  }

  // Features vertical centering
  // -------------------------

  $('.content.section').each(function(){
      var ch = $(this).height() - 150,
          eh = $('.desc', this).height(),
          mh = (ch - eh) / 2;
      $('.desc', this).css("margin-top", mh);
  });


	if($(window).width() <= 768) {
		$('#main-nav .control').on("click", function(e){
			e.preventDefault();
			$('#main-nav .main-nav, #home').toggleClass('open');
		});

  $(window).resize(function(){
      $('.content.section').each(function(){
          var ch = $(this).height() - 150,
              eh = $('.desc', this).height(),
              mh = (ch - eh) / 2;
          $('.desc', this).css("margin-top", mh);
      });
  });

  // BigText.js
  // -------------------------
  $('#home #callout').bigtext({
      childSelector: 'h2'
  });


});

//Change the Skrollr.js data- attributes on Mobile++
	var windowW = $(window).width();
	if( windowW >= 768 &&  windowW <= 1280 ) {

		var searchAndReplace = function(element, removeFirst, removeSecond, First, Second, position1, position2){
			//set element id
			var selector = $(element);
			//remove data-(variable) and replace it with another data- attribute with a background position
			selector.removeAttr('data-' + removeFirst + '').attr('data-'+First+'', position1);
			selector.removeAttr('data-' + removeSecond + '').attr('data-'+Second+'', position2);
		} 
		//Set positions and replace
		var home1 = "background-position: 40% 0px";
		var home2 = "background-position: 40% -600px";
		searchAndReplace("#home", 0, 600, 0, 300, home1, home2);

	}
