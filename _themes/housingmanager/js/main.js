$("img.lazy").lazyload({
    // Lazy load!
    effect : "fadeIn"
});


// Vertical align plugin
// -------------------------

(function ($) {
$.fn.vAlign = function() {
  return this.each(function(i){
  var ah = $(this).height();
  var ph = $(this).parent().height();
  var mh = ((ph-ah) / 2) - 100;
  $(this).css('margin-top', mh);
  });
};
})(jQuery);


$(document).ready(function(){

  if($(window).width() <= 768) {
    $('#main-nav .control').on("click", function(e){
      e.preventDefault();
      $('#main-nav .main-nav, #home, #interior').toggleClass('open');
    });
  }

  // Features vertical centering
  // -------------------------

  $('.content.section').each(function(){
      var ch = $(this).height() - 150,
          eh = $('.desc', this).height(),
          mh = (ch - eh) / 2;
      $('.desc', this).css("margin-top", mh);
  });


	if($(window).width() <= 768) {
		$('#main-nav .control').on("click", function(e){
			e.preventDefault();
			$('#main-nav .main-nav, #home').toggleClass('open');
		});

  $(window).resize(function(){
      $('.content.section').each(function(){
          var ch = $(this).height() - 150,
              eh = $('.desc', this).height(),
              mh = (ch - eh) / 2;
          $('.desc', this).css("margin-top", mh);
      });
  });

  // BigText.js
  // -------------------------
  $('#home #callout').bigtext({
      childSelector: 'h2'
  });


});

