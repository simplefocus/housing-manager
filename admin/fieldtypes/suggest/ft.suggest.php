<?php

class Fieldtype_suggest extends Fieldtype
{
    public function render()
    {
        /*
        |--------------------------------------------------------------------------
        | Multi-select
        |--------------------------------------------------------------------------
        |
        | We need to set an empty array brace [] and add the "multiple" attribute
        | in  the event we want to allow multi-selects. We also change the
        | plurality of the placeholder content.
        |
        */

        $multiple = array_get($this->field_config, 'multiple', true);
        $multiple_array_holder = $multiple ? '[]' : '';

        $suggestions = array();

        /*
        |--------------------------------------------------------------------------
        | Hardcoded list of options
        |--------------------------------------------------------------------------
        |
        | Any list can contain a preset list of options available for suggestion,
        | exactly like how the Select fieldtype works.
        |
        */

        if (isset($this->field_config['options'])) {
            $options = $this->field_config['options'];
            $suggestions = array_merge($suggestions, $options);
        }

        /*
        |--------------------------------------------------------------------------
        | Entries & Pages
        |--------------------------------------------------------------------------
        |
        | Fetch a list of pages and/or entries, using any existing fields as
        | labels and values
        |
        */

        if (isset($this->field_config['content'])) {

            $config = $this->field_config['content'];

            $value   = array_get($config, 'value', 'url');
            $label   = array_get($config, 'label', 'title');
            $folder  = array_get($config, 'folder');


            $content_set = ContentService::getContentByFolders(array($folder));

            $content_set->filter(array(
                    'show_hidden' => array_get($config, 'show_all', false),
                    'show_drafts' => array_get($config, 'show_drafts', false),
                    'since'       => array_get($config, 'since'),
                    'until'       => array_get($config, 'until'),
                    'show_past'   => array_get($config, 'show_past', true),
                    'show_future' => array_get($config, 'show_future', true),
                    'type'        => 'entries',
                    'conditions'  => trim(array_get($config, 'conditions'))
                )
            );
            $entries = $content_set->get();

            foreach ($entries as $key => $entry) {
                if (isset($entry[$label]) && isset($entry[$value])) {
                    $suggestions[$entry[$value]] = $entry[$label];
                }
            }
        }

        /*
        |--------------------------------------------------------------------------
        | Taxonomies
        |--------------------------------------------------------------------------
        |
        | Single taxonomy types can be fetched from any folder. The taxonomy label
        | and value will be identical to ensure consistency with template logic
        |
        */

        if (isset($this->field_config['taxonomy'])) {

            $taxonomy_type = array_get($this->field_config, 'taxonomy:type');
            $folder        = array_get($this->field_config, 'taxonomy:folder');
            $taxonomy_set  = ContentService::getTaxonomiesByType($taxonomy_type);

            // now filter that down to just what we want
            $taxonomy_set->filter(array(
                "folders"   => array($folder)
            ));

            $taxonomy_set->contextualize($folder);
            $taxonomies = $taxonomy_set->get();


            foreach ($taxonomies as $key => $value) {
                $taxonomies[$key] = $value['name'];
            }

            $suggestions = array_merge($suggestions, $taxonomies);
        }

        /*
        |--------------------------------------------------------------------------
        | Input HTML
        |--------------------------------------------------------------------------
        |
        | Generate the HTML for the select field. A single, blank option is
        | needed if in single select mode.
        |
        */

        $html = "<div id='$this->field_id'><select name='{$this->fieldname}' tabindex='{$this->tabindex}' multiple='multiple' class='suggest'>\n";

        foreach ($suggestions as $value => $label) {
            if ($multiple && is_array($this->field_data)) {
                $selected = in_array($value, $this->field_data) ? " selected " : '';
            } else {
                $selected = $this->field_data == $value ? " selected " : '';
            }
            $html .= "<option value='{$value}'{$selected}>{$label}</option>\n";
        }

        $html .= "</select>";
        $html .= "<div class='count-placeholder'></div></div>";

        /*
        |--------------------------------------------------------------------------
        | The JavaScript
        |--------------------------------------------------------------------------
        |
        | Set the config options, instantiate Selectize, and so forth.
        |
        */

        $max_items = array_get($this->field_config, 'max_items', 'null');
        if ($max_items === null && $multiple === false) {
            $max_items = 1;
        }

        $options = json_encode(array(
            'sortField'      => 'text',
            'maxItems'       => $max_items,
            'create'         => array_get($this->field_config, 'create', false),
            'persist'        => array_get($this->field_config, 'persist', true),
            'hideSelected'   => array_get($this->field_config, 'hide_selected', true),
            'sortDirection'  => array_get($this->field_config, 'sort_dir', 'asc'),
            'plugins'        => array('drag_drop'),
            'dropdownParent' => 'body'
        ));

        $html .= "
        <script>
        $(function() {

            var selectize = $('#$this->field_id select'),
                options = $options,
                value = selectize.val(),
                max_items = $max_items;


            // @TODO: Rewrite in KO to avoid scoping issues in Grid fields
            // if (max_items != null) {
            //    var count = (value === null) ? 0 : value.length,
            //        remaining = max_items - count,
            //        placeholder = $('#$this->field_id .count-placeholder');
            //     $.extend(options, {
            //             'onChange': function(value) {
            //                 count = (value === null) ? 0 : value.length;
            //                 remaining = max_items - count;
            //                 placeholder.text(remaining + ' remaining');
            //             }
            //         }
            //     );
            //     placeholder.text(remaining + ' remaining');
            // }

            $(selectize).selectize(options);
        });

        </script>
        ";

        return $html;
    }


    public function process()
    {
        // If there's only one option
        // save it as a string instead of an array

        if (count($this->field_data) === 1) {
            return $this->field_data[0];
        }

        return $this->field_data;
    }
}
