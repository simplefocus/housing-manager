---
title: Home
_fieldset: home
_layout: default
feature_caption_1: "There's a better way."
feature_text_1: "We've made HousingManager.com easy to learn and simple to use while still giving you the power you need to effectively manage your rentals. No matter how many applications you're planning to accept, HousingManager.com has a pricing option that will fit your needs."
feature_link_title_1: Take a tour of our features
feature_link_href_1: /features
feature_caption_2: Create custom applications.
feature_text_2: >
  HousingManager.com makes it easier for
  you to find the perfect potential
  residents by allowing you to build
  custom rental applications. Residents
  can apply online, eliminating paperwork.
feature_link_title_2: Learn More
feature_link_href_2: /features
feature_caption_3: No installation required.
feature_text_3: >
  Because HousingManager.com is web based,
  you can easily access it from any
  computer, tablet or smart phone without
  installing any software.
feature_link_title_3: 'Learn More '
feature_link_href_3: /features
callout_title: Simple, powerful rental applications.
callout_link_title: Learn More
callout_link_href: /features
---

