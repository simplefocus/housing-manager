---
title: Features
_fieldset: features
_layout: interior
_template: features
page_caption: "Easily manage all of your company's properties with one application."
page_text: "Whether you're in charge of one property or dozens of them, HousingManager.com gives you the ability to manage all of them from one location. Take a look at what HousingManager.com allows you to do:"
features:
  - 
    caption: Create custom applications.
    some_text: >
      HousingManager.com makes it easier for
      you to find the perfect potential
      residents by allowing you to build
      custom rental applications. Residents
      can apply online, eliminating paperwork.
    feature_image: /assets/img/feature-1.jpg
  - 
    caption: Allow residents to apply online.
    some_text: |
      Once you’ve created an application, potential residents can fill it out and turn it in from any computer,
      eliminating the need for piles of paperwork.
    feature_image: /assets/img/feature-2.jpg
  - 
    caption: Sort and manage applications.
    some_text: >
      HousingManager.com’s application
      manager gives you a hassle-­free way to
      sort applications by the criteria
      you’ve created and approve or deny
      them.
    feature_image: /assets/img/feature-3.jpg
descriptions:
  - 
    caption: No installation required.
    some_text: >
      Because HousingManager.com is web based,
      you can easily access it from any
      computer, tablet or smart phone without
      installing any software.
    feature_image: /assets/img/icon/no-install-icon.png
  - 
    caption: Easy to use.
    some_text: >
      It takes time and patience to learn new
      things. We understand that, so we’ve
      made HousingManager.com intuitive and
      easy to use so that you can quickly
      master it and get back to work.
    feature_image: /assets/img/icon/easy-use.png
  - 
    caption: Application fees.
    some_text: >
      With a HousingManager.com merchant
      account, you can allow your potential
      tenants to quickly and easily pay their
      application fees online.
    feature_image: /assets/img/icon/app-fees.png
  - 
    caption: Varying levels of access.
    some_text: >
      Give your employees the power to create
      and approve applications, place new
      residents and do background checks. Or
      not. HousingManager.com allows you to
      have variable levels of access so that
      your staff can access only the tools
      that they need.
    feature_image: /assets/img/icon/levels-icon.png
---


















<<<<<<< HEAD
=======



>>>>>>> f17371ef885ccadb4f1a3a13aa5dd78253dc5f41
