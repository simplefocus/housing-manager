---
_fieldset: content
title: Privacy Policy
page_caption: Our Privacy Policy
_layout: interior
_template: content
status: hidden
---
Privacy Statement
This Privacy Policy governs the web site accessed at the URL http://HousingManager.com (the “Site”). By visiting the Site, you expressly consent to the collection and use of personally identifiable information according to the terms and conditions of this Privacy Policy.

When you send us e-mail, we will retain the content of the e-mail, your e-mail address, and our response in order to handle any follow-up questions you may have. We also use this information internally to measure how effectively we address your concerns.

We only send e-mail messages to you if you have voluntarily provided us with your e-mail address.

We do not sell your name or other private profile information to third parties, and do not intend to do so in the future. We may share anonymous, aggregated information about all our visitors with third parties.

Please note that the Site contains links to other web sites on the Internet. We cannot control and are not responsible for the privacy practices of the proprietors of those web sites. We encourage you to read the privacy statements of each web site that collects information from you. This Privacy Policy applies solely to information collected through your access to and use of the Site.

You may review any personal information we have collected about you by sending an email to john_lindsey@lindseysoftware.com. You may edit this information to correct inaccuracies or delete information by sending an email to the same address.

We have implemented industry standard security measures to protect against the loss, misuse, or alteration of information you provide to us through the Site. While there is no such thing as perfect security measures on the Internet, we will take reasonable steps to ensure the safety of your personal information.

We understand the importance of protecting children’s privacy, especially in an online environment. The Site is not directed to children under the age of 13, and we do not knowingly collect personal information from or about children. If you are under the age of 18, you may only access and use this Site through the involvement of a parent or guardian.

We reserve the right to modify this Privacy Policy at any time without prior notice. If we change or update our Privacy Policy, we will post the revised policy here, and any changes will apply to all information collected, including previously collected information.

Terms of Use
The content of the Lindsey Software Site is made available for limited non-commercial, educational and personal use only, or for fair use as defined in the United States copyright laws. Users may download these files for their own use, subject to any additional terms or restrictions which may be applicable to the individual file or program.

Copying or redistribution in any manner for commercial use, including commercial publication, or for personal gain is strictly prohibited.