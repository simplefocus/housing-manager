---
title: Free Trial
_fieldset: free-trial
_layout: interior
_template: free-trial
page_caption: Start your free trial today.

page_text: >
  No matter how many applications you plan
  on accepting, Housing Manager has a
  pricing option to fit your needs. If
  you’re not sure how many applications
  you’ll have, it’s alright. Just pick
  a plan, and if you go over your allotted
  amount, we’ll bill you for the overage
  at the end of the month that it occurs.

features:
  - 
    name: Small
    price: $499
    description: 500 applications annually
  - 
    name: Medium
    price: $999
    description: 1000 applications annually
  - 
    name: Large
    price: $1999
    description: 2000 applications annually
  - 
    name: Enterprise
    price: $4999
    description: 5000 applications annually
---


